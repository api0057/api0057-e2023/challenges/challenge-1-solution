from pathlib import Path

import numpy as np

from distances import minimal


def test_minimal_distance():
    x_points = np.loadtxt(Path(__file__).parent.joinpath("points1.txt"))
    y_points = np.loadtxt(Path(__file__).parent.joinpath("points2.txt"))

    dist_min = minimal(x_points, y_points)

    assert np.isclose(dist_min, 0.014142135623730963)
